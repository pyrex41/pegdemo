# seems very inneficient but good enough for some quick and dirty exploring
function monte_carlo(ts, pr_ret, distr, N=1000; kwargs...)
    n = length(ts)
    @assert n == length(pr_ret)
    pl = [zeros(n) for _ in 1:N]
    pg = [zeros(n) for _ in 1:N]
    pr = [zeros(n) for _ in 1:N]
    function func(i)
        dd = DataFrame(timestamp = ts, pool_growth = rand(distr, n), peg_growth = rand(distr, n), price_return = shuffle(pr_ret))
        core,out = sim_days(dd; kwargs...)
        res = getdata(dd, out)
        pl[i] = res.pool_eth
        pg[i] = res.peg_eth
        pr[i] = res.price
    end
    1:N |> Map(func) |> tcollect
    pl, pg, pr
end

# for plotting, 10th, 50th, 90th percentile paths
function indic_paths(avec)
    savec = sort(avec, by=x->last(x))
    tenth = round(.1 * length(avec)) |> Int
    ninetieth = round(.9 * length(avec)) |> Int
    mid = round(.5 * length(avec)) |> Int
    avec[tenth], avec[mid], avec[ninetieth]
end
