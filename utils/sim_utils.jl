# `minter` must be defined in current environment

function init_model(df::DataFrame; kwargs...)
    core = core_init(minter; kwargs...)
    m1 = Model(core, df.timestamp[1])
    counter = 1
    maxcount = length(df.timestamp)
    arr = [m1]
    function step!()
        if counter < maxcount
            counter += 1
            m = sim_1_day(counter, df, core, minter)
            push!(arr, m)
        else
            @warn "Counter reached maximum"
        end
    end
    core, arr, step!
end

function sim_days(df::DataFrame; verbose=false, logfile=logfile, kwargs...)
    if verbose
        logger = ConsoleLogger(stderr, Logging.Debug)
        sim_days(df, logger)
    else
        io = open(logfile, "w+")
        logger = SimpleLogger(io)
        core, arr = sim_days(df, logger; kwargs...)
        flush(io)
        close(io)
        core, arr
    end
end

function sim_days(df::DataFrame, logger::T; kwargs...) where { T<:AbstractLogger}
    core, arr, step! = init_model(df; kwargs...)
    n,_ = size(df)
    with_logger(logger) do
        for i=1:n
            step!()
        end
    end
    core, arr
end


function getdata(dff, out)
    x = 1:size(dff)[1]
    df = DataFrame(peg_issued = Float64[], peg_eth = Float64[], pool_issued = Float64[],
                   pool_eth = Float64[], price = Float64[], core_balance = Float64[],
                   total_eth = Float64[], leverage = Float64[], timestamp = Date[])
    fnames = fieldnames(Model)
    x |> Map(i-> out[i]) |> Map(sn -> map(x->getfield(sn, x), fnames)) |> Map(data -> push!(df, data)) |> collect
    df
end
