# include("sim.jl")

using CairoMakie

function make_plot(out, name="out.png")
    x = 1:size(res)[1]
    y1 = out.peg_eth
    y2 = out.pool_eth
    y3 = out.leverage

    f = Figure()
    ax1 = Axis(f[1,1])
    ax1.ylabel = "# ETH"
    ax1.xlabel = "N Days"
    ax2 = Axis(f[1,1], yticklabelcolor=:purple, yaxisposition=:right)
    ax2.ylabel = "Leverage Ratio"
    hidespines!(ax2)
    hidexdecorations!(ax2)

    l1 = lines!(ax1, x,y1,color=:blue, label=:peg)
    l2 = lines!(ax1, x,y2,color=:red, label=:pool)
    l3 = lines!(ax2, x, y3, color=:purple, label=:leverage)
    axislegend(ax1, [l1,l2,l3], ["PEG ETH", "POOL ETH", "LEV Ratio"], position = :ct)

    save(name, f, px_per_unit=10)
    @info "Plot saved as: $name"
end

function qp(df::DataFrame)
    x = size(df)[1]
    f(a,b) = a * (1 + b)
    y1 = df.peg_growth |> Scan(f, 1) |> collect
    y2 = df.pool_growth |> Scan(f, 1) |> collect
    y3 = df.price_return |> Scan(f, df.price[1]) |> collect
    qp(collect(1:x),y1,y2,y3)
end
function qp(x,y...)
    f = Figure()
    ax = Axis(f[1,1])
    for y_ in y
        lines!(ax, x, y_)
    end
    save("out.png", f, px_per_unit=10)
    f
end

function mcp(pl, pg, pr)
    a, b, c = map(indic_paths, (pl, pg, pr))

    x =collect(1:length(a[1]))
    f = Figure()
    ax = Axis(f[1,1])
    ax2 = Axis(f[1,1], yticklabelcolor=:purple, yaxisposition=:right)
    hidespines!(ax2)
    hidexdecorations!(ax2)

    map(a) do y
        lines!(ax, x, y, color=:blue)
    end
    map(b) do y
        lines!(ax, x, y, color = :red)
    end
    map(c) do y
        lines!(ax2, x, y, color=:purple)
    end
    save("out.png", f, px_per_unit=10)
end
