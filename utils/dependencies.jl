import Pkg

tempdir = mktempdir()

Pkg.activate(tempdir)
pkgs = ["TimeSeries", "DataFrames", "Transducers", "Statistics", "Logging", "CSV",
        "Distributions", "Revise", "Random"]

# uncomment if you need the graphing tools
#pkgs = push!(pkgs, "CairoMakie")

Pkg.add(pkgs)
