# include("sim.jl")
# assumes you have already run the "sim.jl" script

using CairoMakie
include("utils/plot_utils.jl")

struct StockValue{T<:Real}
    open::T
    high::T
    low::T
    close::T
end
@recipe(StockChart) do scene
    Attributes(
        downcolor = :red,
        upcolor = :green,
    )
end

function Makie.plot!(
        sc::StockChart{<:Tuple{AbstractVector{<:Real}, AbstractVector{<:StockValue}}})

    # our first argument is an observable of parametric type AbstractVector{<:Real}
    times = sc[1]
    # our second argument is an observable of parametric type AbstractVector{<:StockValue}}
    stockvalues = sc[2]

    # we predefine a couple of observables for the linesegments
    # and barplots we need to draw
    # this is necessary because in Makie we want every recipe to be interactively updateable
    # and therefore need to connect the observable machinery to do so
    linesegs = Observable(Point2f[])
    bar_froms = Observable(Float32[])
    bar_tos = Observable(Float32[])
    colors = Observable(Bool[])

    # this helper function will update our observables
    # whenever `times` or `stockvalues` change
    function update_plot(times, stockvalues)
        colors[]

        # clear the vectors inside the observables
        empty!(linesegs[])
        empty!(bar_froms[])
        empty!(bar_tos[])
        empty!(colors[])

        # then refill them with our updated values
        for (t, s) in zip(times, stockvalues)
            push!(linesegs[], Point2f(t, s.low))
            push!(linesegs[], Point2f(t, s.high))
            push!(bar_froms[], s.open)
            push!(bar_tos[], s.close)
        end
        append!(colors[], [x.close > x.open for x in stockvalues])
        colors[] = colors[]
    end

    # connect `update_plot` so that it is called whenever `times`
    # or `stockvalues` change
    Makie.Observables.onany(update_plot, times, stockvalues)

    # then call it once manually with the first `times` and `stockvalues`
    # contents so we prepopulate all observables with correct values
    update_plot(times[], stockvalues[])

    # for the colors we just use a vector of booleans or 0s and 1s, which are
    # colored according to a 2-element colormap
    # we build this colormap out of our `downcolor` and `upcolor`
    # we give the observable element type `Any` so it will not error when we change
    # a color from a symbol like :red to a different type like RGBf(1, 0, 1)
    colormap = lift(Any, sc.downcolor, sc.upcolor) do dc, uc
        [dc, uc]
    end

    # in the last step we plot into our `sc` StockChart object, which means
    # that our new plot is just made out of two simpler recipes layered on
    # top of each other
    linesegments!(sc, linesegs, color = colors, colormap = colormap)
    barplot!(sc, times, bar_froms, fillto = bar_tos, color = colors, strokewidth = 0, colormap = colormap)
    # lastly we return the new StockChart
    sc
end


function get_ohlc(i)
    ts = dd.timestamp[i]
    StockValue(values(price_data[ts])[1:4]...)
end

@info "Setting up plot"

n=50

w = 80
timestamps = 1:n
nmax,_ = size(res)


f = Figure()
ax1 = Axis(f[3,1], yticklabelcolor=:blue)
ax1.ylabel = "# ETH"
ax1.xlabel = "N Days"
ax2 = Axis(f[3,1], yaxisposition = :right)
hidespines!(ax2)
hidexdecorations!(ax2)
hideydecorations!(ax2)
ax2b = Axis(f[3,1], yaxisposition = :right)
hidespines!(ax2b)
hidexdecorations!(ax2b)
hideydecorations!(ax2b)
y1a = res.peg_eth[1:n] |> Observable
y1b = res.peg_issued[1:n] |> Observable
y2a = res.pool_eth[1:n] |> Observable
y2b = res.pool_issued[1:n] |> Observable


ts2 = Observable(collect(timestamps))
l1 = lines!(ax1, ts2, y1a)
l2 = lines!(ax1, ts2, y2a, color=:red)
l3 = lines!(ax2, ts2, y1b, color="#73d0f4")
l4 = lines!(ax2b, ts2, y2b, color = "#6b030a")
axislegend(ax1, [l1,l2, l3, l4], ["PEG ETH", "POOL ETH", "PEG Issued", "POOL Issued"], position=:lt)

ts = Observable(timestamps |> collect)
sv = Observable(map(get_ohlc, timestamps))
uk = stockchart(f[1,:], ts, sv)
uk.axis.ylabel = "\$ / ETH"


sv2 = Observable(map(get_ohlc, timestamps))
uk2 = stockchart(f[2,:], ts2, sv2)
uk2.axis.ylabel = "\$ / ETH"

y3 = Observable(res.leverage[1:n])
ax3 = Axis(f[1,:], yticklabelcolor=:purple, yaxisposition=:right)
hidespines!(ax3)
hidexdecorations!(ax3)
lines!(ax3, ts, y3, color=:purple)

y4 = Observable(res.leverage[1:n])
ax4 = Axis(f[2,:], yticklabelcolor=:purple, yaxisposition=:right)
hidespines!(ax4)
hidexdecorations!(ax4)
l4 = lines!(ax4, ts2, y4, color=:purple)
axislegend(ax4, [l4], ["Leverage Ratio"], position = :lt)

function step_forward(t; slide=0)
    push!(ts[], t)
    push!(ts2[], t)
    nv = get_ohlc(t)
    push!(sv[], nv)
    push!(y1a[], res.peg_eth[t])
    push!(y1b[], res.peg_issued[t])
    push!(y2a[], res.pool_eth[t])
    push!(y2b[], res.pool_issued[t])
    push!(sv2[], nv)
    push!(y3[], res.leverage[t])
    push!(y4[], res.leverage[t])

    if 0 < slide < Inf
        while length(ts[]) > slide
            popfirst!(ts[])
            popfirst!(sv[])
            popfirst!(y3[])
            @assert length(ts[]) == length(sv[]) == length(y3[])
        end
    end

    y1a[] = y1a[]
    y2a[] = y2a[]
    autolimits!(ax1)

    y1b[] = y1b[]
    autolimits!(ax2)
    y2b[] = y2b[]
    autolimits!(ax2b)

    sv[] = sv[]
    autolimits!(uk.axis)

    sv2[] = sv2[]
    autolimits!(uk2.axis)

    y3[] = y3[]
    autolimits!(ax3)

    y4[] = y4[]
    autolimits!(ax4)
end

video_name = "layout_animation.mp4"
make_video() = let
    @info "Begining to plot video"
    record(f, video_name, n+1:nmax, framerate = 24) do t
        step_forward(t, slide=80)
    end
    @info string("Video plot saved as ", video_name)
end

simple_plot(name="simple_plot.png") = make_plot(res, name)

println("to generate video, enter `make_video()`")
println("to gnerate a simple plot, enter `simple_plot()`")
