include("utils/dependencies.jl")

using TimeSeries, Transducers, Statistics, DataFrames, Logging, CSV, Distributions,
    Random

include("PEG_DEMO.jl")
using .PEG_DEMO

import Base.max

include("utils/sim_utils.jl")

const logfile = "output_data/log.txt"

dai_data = readtimearray("input_data/dai_data.csv")
price_data = readtimearray("input_data/ETH-USD.csv")

pd = let
    c_vals = price_data[:Close] |> values
    ret = (c_vals[2:end] .- c_vals[1:end-1]) ./ c_vals[1:end-1]
    data = (datetime= timestamp(price_data)[2:end], price_return = ret, Close = c_vals[2:end])
    TimeArray(data; timestamp = :datetime)
end
data = merge(dai_data, pd, method=:inner)

minter = Wallet("0x1", BigInt(1e12) * 1e18)

function r(vec::Vector, distr = Cauchy)
    dist = fit(distr, vec)
    n = length(vec)
    rand(dist,n)
end
function generate_sim(ta; randomize_deposits=true, randomize_prices=true, kwargs...)
    vec = values(ta.mb)
    pool_vec = randomize_deposits ? r(vec) : vec
    peg_vec = randomize_deposits ? r(vec) : vec
    prvec = values(ta.price_return)
    pvec = randomize_prices ? shuffle(prvec) : prvec
    DataFrame(timestamp = timestamp(ta), pool_growth = pool_vec, peg_growth = peg_vec, price_return = pvec)
end

@info "Beginning simulation"
run_sim(name="simulation_output.csv"; kwargs...)  = begin
    try
        global dd = generate_sim(data; kwargs...)
        global core, out = sim_days(dd; peg_init = 500, pool_init = 1000, lev_limit=.8, price_init=175)
        global res = getdata(dd, out)
        CSV.write(name, out)
        @info "Simulation output saved successfully"
    catch e
        @warn e
    end
end
run_sim("output_data/fully_random.csv") # shuffles prices, randomly draws depostis from  distribution fitted to DAI
run_sim("output_data/historical_path.csv"; randomize_deposits=false, randomize_prices=false)
