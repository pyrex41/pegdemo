include("utils/dependencies.jl") #

using TimeSeries, Transducers, Statistics, DataFrames, Logging, CSV, Distributions,
    Random

include("PEG_DEMO.jl")
using .PEG_DEMO


minter = Wallet("0x1", BigInt(1e12) * 1e18) # whale account


core = core_init(minter, lev_limit=.8, peg_init = 500, pool_init = 500,
                 price_init = 2900)
println("Here's `core`, an instance representing the Core smart contract that implements the PEG system logic.")
function show_state()
    println("----------------------------")
    println("State:")
    @show core.balance
    @show core.peg.totalSupply
    @show core.pool.totalSupply
    @show core.price
    @show core.leverage_limit
    @show core.mint_bonus
    @show core.redemption_fee
    @show core.peg_dwei_nominal()
    @show core.peg_dwei_nominal()
    @show core.peg_wei_nominal()
    @show core.peg_required_reserve_wei()
    @show core.peg_actual_reserve_wei()
    @show core.pool_available_wei()
    @show core.pool_wei_nominal()
    @show core.leverage()
    println("----------------------------")
    return
end
println("To show the current state, use `show_state()`")

pl() =     println("----------------------------")
function pgs(n=5)
    pl()
    for i=1:5
        println()
    end
    sleep(n)
end
function demo()
    global core = core_init(minter, lev_limit=.8, peg_init = 500, pool_init = 500,
                 price_init = 2900)
    show_state()
    pgs()
    println("Let's try to redeem some POOL")
    @show core.redeem_pool(1.2e18, minter, minter)

    println("Note how this changes the leverage. If we are above the leverage limit, we can  no longer withdraw")
    @show core.leverage()
    pgs()

    println("Let's change the leverage limit")
    @show core.leverage_limit = 400_000 # now above Leverage limit
    @show core.leverage()
    pgs()

    println("Note that value used in redeeming POOL or PEG is nominal units of that token, not ETH")
    try
        @show core.redeem_pool(1e18, minter, minter)
    catch e
        println("core.redeem_pool(1e18, minter, minter)")
        @show e
    end
    pgs()
    println("We can still mint POOL though:")
    @show core.mint_pool(1e18, minter, minter)

    pgs()
    println("We cannot mint more PEG though:")
    try
        @show core.mint_peg(1e18, minter, minter)
    catch e
        println("core.mint_peg(1e18, minter, minter)")
        @show e
    end
    pgs()
    println("We can redeem PEG always:")
    println("Note that value used in redeeming POOL / PEG is nominal units of that token, not ETH")
    @show core.redeem_peg(10e18, minter, minter)

    pgs()
    println("If we move the Leverage Limit back up though, we can once again mint PEG and redeem POOL:")
    @show core.leverage_limit = 800_000
    @show core.leverage()
    sleep(2)
    @show core.mint_peg(1e18, minter, minter)
    sleep(2)
    @show core.redeem_pool(1e18, minter, minter)
    pgs()

    println("See also how changes in price affect leverage:")
    @show core.price
    @show core.leverage()
    pgs()

    @show core.price = 1.25 * core.price
    @show core.leverage()
    pgs()

    @show core.price = .5 * core.price
    @show core.leverage()
    pgs(3)

    show_state()
    pgs()

    println("We can also send PEG (or POOL):")
    @show alice = Wallet("0xb", 0)
    @show core.peg.balanceOf(minter)
    @show core.peg.balanceOf(alice)
    pgs()

    @show core.peg.send(1e18, minter, alice)
    @show core.peg.balanceOf(minter)
    @show core.peg.balanceOf(alice)
end
println("To run a quick demonstration, type `demo()`")
