module PEG_DEMO

using DataFrames, Dates

export Address, Wallet, MBT, mint, burn, CoreContract, redeem_peg, redeem_pool,
    mint_peg, mint_pool, Model, sim_1_day, price_convert_int, core_init

const WEI_DIGITS = BigInt(1_000_000_000_000_000_000)
const MAX_BPS = 10_000
const PRICE_DECIMAL = 1_000_000
const ETH_PRICE_DECIMAL = 100_000_000

bi(x::Integer) = BigInt(x)
bi(x::Real) = BigInt(round(x))
price_convert_int(x) = ETH_PRICE_DECIMAL * x |> bi

abstract type Address end # all Address instances need a balance field
send(a,b,c) = send(a,b, bi(c))
function send(_from::T, _to::Z, amt::BigInt) where {T <: Address, Z <: Address}
    _from.balance -= amt
    _to.balance += amt
    amt
end

mutable struct Wallet <: Address
    address::AbstractString
    balance::BigInt
end
Wallet(ad, b=0) = Wallet(ad, bi(b))

mutable struct MBT <: Address
    name::Symbol
    totalSupply::BigInt
    balance::BigInt
    balances::Dict{AbstractString, BigInt}
    balanceOf::Function
    send::Function
end
function MBT(name)
    balances = Dict{AbstractString, BigInt}()
    b(x, dic) = get(dic, x, 0)
    b(x::Wallet, dic) = get(dic, x.address, 0)
    m = MBT(
        name,
        BigInt(0),
        BigInt(0),
        balances,
        (x) -> b(x, m.balances),
        (amount_, sender, recipient) -> send(m, amount_, sender, recipient)
    )
end

function send(token::MBT, amount_::BigInt, sender::T, recipient::Y) where {T <: Address, Y <: Address}
    @assert token.balanceOf(sender) >= amount_
    token.balances[recipient.address] = get(token.balances, recipient.address, 0) + amount_
    token.balances[sender.address] -= amount_
    amount_
end
send(token::MBT, amount_::Real, sender, recipient) = send(token, bi(amount_), sender, recipient)

function mint(wallet::T, amt, mbt::MBT) where {T <: Address}
    address = wallet.address
    old_amount = get(mbt.balances, address, BigInt(0))
    old_amount = old_amount
    mbt.balances[address] = old_amount + amt
    mbt.totalSupply += amt
    amt
end

function burn(wallet::T, amt, mbt::MBT) where {T <: Address}
    address = wallet.address
    old_amount = get(mbt.balances, address, 0)
    old_amount = old_amount
    amount_destroy = old_amount - amt > 0 ? amt : old_amount
    mbt.balances[address] = old_amount - amount_destroy
    mbt.totalSupply -= amount_destroy
    amount_destroy
end

mutable struct CoreContract <: Address
    peg::MBT
    pool::MBT
    price::BigInt
    leverage_limit::Int
    mint_bonus::Int
    balance::BigInt  # total ETH
    redemption_fee::BigInt
    leverage::Function
    peg_dwei_nominal::Function
    peg_wei_nominal::Function
    peg_required_reserve_wei::Function
    peg_actual_reserve_wei::Function
    pool_available_wei::Function
    pool_wei_nominal::Function
    redeem_peg::Function
    mint_peg::Function
    redeem_pool::Function
    mint_pool::Function
end
function CoreContract(price, leverage_limit, mint_bonus, balance, redemption_fee)
    c = CoreContract(
        MBT(:peg),
        MBT(:pool),
        price,
        leverage_limit,
        mint_bonus,
        balance,
        redemption_fee,
        () -> leverage(c),
        () -> peg_dwei_nominal(c),
        () -> peg_wei_nominal(c),
        () -> peg_required_reserve_wei(c),
        () -> peg_actual_reserve_wei(c),
        () -> pool_available_wei(c),
        () -> pool_wei_nominal(c),
        (_value, sender, recipient) -> redeem_peg(_value, sender, recipient, c),
        (_value, sender, recipient) -> mint_peg(_value, sender, recipient, c),
        (_value, sender, recipient) -> redeem_pool(_value, sender, recipient, c),
        (_value, sender, recipient) -> mint_pool(_value, sender, recipient, c),
    )
    c
end

# nominal -- as if all peg gets redeemed at current price
# dwei * 1e18 == $
peg_dwei_nominal(core::CoreContract) = core.peg.totalSupply
peg_wei_nominal(core::CoreContract) = core.peg.totalSupply * ETH_PRICE_DECIMAL ÷ core.price

# required reserve collateral.
# If core.leverage_limit < 1, then this will be > peg_wei_nominal
# If core.leverage_limit > 1, then this will be < peg_wei_nominal (fractional reserve system)
peg_required_reserve_wei(core::CoreContract) = peg_wei_nominal(core) * PRICE_DECIMAL / core.leverage_limit
peg_actual_reserve_wei(core::CoreContract) = min(peg_required_reserve_wei(core), core.balance)

# Allowable withdrawals for POOL holders while mainting required capital for PEG holders per the leverage limit
pool_available_wei(core::CoreContract) = max(core.balance - peg_required_reserve_wei(core), 0)
# Calculated value of POOL for valuation
pool_wei_nominal(core::CoreContract) = core.balance - peg_wei_nominal(core)

# Actual leverage at current price
leverage(core::CoreContract) = peg_wei_nominal(core) * PRICE_DECIMAL ÷ core.balance

function redeem_peg(_value, sender::Address, recipient::Address, core::CoreContract)
    amt = min(_value, core.peg.balanceOf(sender))
    @assert amt > 0
    collateral_raw = amt * ETH_PRICE_DECIMAL ÷ core.price
    fee = collateral_raw * core.redemption_fee ÷ MAX_BPS
    collateral_return = collateral_raw - fee

    burn(sender, amt, core.peg)
    send(core, recipient, collateral_return)
    collateral_return
end

function mint_peg(_value, sender::Address, recipient::Address, core::CoreContract)
    price = core.price
    msg_dollar = _value * price ÷ ETH_PRICE_DECIMAL * (MAX_BPS + core.mint_bonus) ÷ MAX_BPS

    new_peg = core.peg.totalSupply + msg_dollar
    new_peg_wei = peg_wei_nominal(core) + _value
    new_balance = core.balance + _value
    new_lev = new_peg_wei * PRICE_DECIMAL ÷ new_balance
    @assert new_lev <= core.leverage_limit

    mint(recipient, msg_dollar, core.peg)
    send(sender, core, _value)
    msg_dollar
end

function pool_to_wei(amt_pool, core::CoreContract)
    ts = core.pool.totalSupply
    ts == 0 ? BigInt(0) : amt_pool * core.pool_available_wei() ÷ ts |> bi
end

mint_pool(_value_eth, sender::Address, recipient::Address, core::CoreContract) = mint_pool(bi(_value_eth), sender, recipient, core)
function mint_pool(_value_eth::BigInt, sender::Address, recipient::Address, core::CoreContract)
    pool_eth = core.pool_wei_nominal()
    delta_pool = if core.pool.totalSupply == 0 || pool_eth == 0
        _value_eth
    else
        core.pool.totalSupply * _value_eth ÷ pool_eth
    end

    delta_pool = bi(delta_pool)
    mint(recipient, delta_pool, core.pool)
    send(sender, core, _value_eth)
    delta_pool
end


function redeem_pool(_value, sender::Address, recipient::Address, core::CoreContract)
    amt_pool = min(_value, core.pool.balanceOf(sender))
    @assert amt_pool > 0
    available_eth = core.pool_available_wei()
    @assert available_eth > 0
    requested_eth = pool_to_wei(amt_pool, core)
    amt_eth = min(available_eth, requested_eth)

    burn(sender, amt_pool, core.pool)
    send(core, recipient, amt_eth)
    amt_eth
end

Base.@kwdef struct Model
    peg_issued::BigInt
    peg_eth::Real
    pool_issued::BigInt
    pool_eth::Real
    price::Real
    core_balance::BigInt
    total_eth::Real
    leverage::Real
    ts::Date
end
Model(core::CoreContract, dt=today()) = Model(
    core.peg.totalSupply,
    core.peg_wei_nominal() / 1e18 |> Float64,
    core.pool.totalSupply,
    core.pool_wei_nominal() / 1e18 |> Float64,
    round(core.price / ETH_PRICE_DECIMAL, digits=2) |> Float64,
    core.balance,
    core.balance / 1e18 |> Float64,
    core.leverage() / PRICE_DECIMAL,
    dt
)

function core_init(minter; lev_limit, peg_init, pool_init, price_init)
    core = CoreContract(
        price_init * ETH_PRICE_DECIMAL ÷ 1 |> Int,
        lev_limit * PRICE_DECIMAL ÷ 1 |> Int,
        0,
        0,
        0
    )
    mint_pool(pool_init * 1e18, minter, minter, core)
    mint_peg(peg_init * 1e18, minter, minter, core)
    core
end


function sim_1_day(i::Integer, df::DataFrame, args...)
    timestamp = df[i, :timestamp]
    price_return = df[i, :price_return]
    pool_growth = df[i, :pool_growth]
    peg_growth = df[i, :peg_growth]
    sim_1_day(timestamp, price_return, pool_growth, peg_growth, args...)
end

function sim_1_day(timestamp, price_return, pool_growth, peg_growth, core::CoreContract, minter::Address)

    price_delta = Int(core.price * price_return ÷ PRICE_DECIMAL) * PRICE_DECIMAL
    core.price = core.price + price_delta

    ba = core.balance
    let
        amt = Int(core.pool_wei_nominal()  * pool_growth ÷ PRICE_DECIMAL) * PRICE_DECIMAL
        if amt < 0
            try
                core.redeem_pool(amt * -1, minter, minter)
                @info "Burned $amt Pool"
            catch e
                # check this
                @warn string(e)
            end
        else
            try
                core.mint_pool(amt, minter, minter)
                @info "Minted $amt Pool"
            catch e
                @warn string(e)
            end
        end
    end

    let
        amt = Int(core.peg_wei_nominal() * peg_growth ÷ PRICE_DECIMAL) * PRICE_DECIMAL
        if amt < 0
            try
                core.redeem_peg(amt * -1, minter, minter)
                @info "Burned $amt PEG"
                # check this
            catch e
                @warn string(e)
            end
        else
            try
                core.mint_peg(amt, minter, minter)
                @info "Minted $amt PEG"
            catch e
                @warn string(e)
            end
        end
    end
    Model(core, timestamp)
end
end # end module
