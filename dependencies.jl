import Pkg

tempdir = mktempdir()

Pkg.activate(tempdir)
pkgs = ["TimeSeries", "DataFrames", "Transducers", "Statistics", "Logging", "CSV",
        "CairoMakie", "Distributions", "Revise", "Random"]
Pkg.add(pkgs)
