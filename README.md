The demo is written in Julia. You can install Julia from https://julialang.org/downloads/

I have tested this on a Mac; should work on Linux, but not sure about Windows.

## System Simulation

![Simulation Video](layout_animation.mp4)

## Simplified Implementation in Code

Simplified toy model of the PEG mechanics are found in the [PEG_DEMO](PEG_DEMO.jl) module.

## Interactive Demo

Run live demo, use the `demo.jl` script in interactive mode:

> julia -i demo.jl

To run the demo, type `demo()`:

``` julia
Here's `core`, an instance representing the Core smart contract that implements the PEG system logic.
To show the current state, use `show_state()`
To run a quick demonstration, type `demo()`
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.7.2 (2022-02-06)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

julia> demo()
----------------------------
State:
core.balance = 1000000000000000000000
core.peg.totalSupply = 1450000000000000000000000
core.pool.totalSupply = 500000000000000000000
core.price = 290000000000
core.leverage_limit = 800000
core.mint_bonus = 0
core.redemption_fee = 0
core.peg_dwei_nominal() = 1450000000000000000000000
core.peg_dwei_nominal() = 1450000000000000000000000
core.peg_wei_nominal() = 500000000000000000000
core.peg_required_reserve_wei() = 6.25e+20
core.peg_actual_reserve_wei() = 6.25e+20
core.pool_available_wei() = 3.75e+20
core.pool_wei_nominal() = 500000000000000000000
core.leverage() = 500000
----------------------------
----------------------------


```

In interactive mode, you can create and destroy POOl and PEG, and query the contract
object (`core`) to inspect the current state:

``` julia
julia> core.mint_peg(1e18, minter, minter)
2.9e+21

julia> core.redeem_peg(5e17, minter, minter)
1.72413793103448e+14

julia> core.mint_pool(3e18, minter, minter)
2999999999999999999

julia> core.redeem_pool(2e18, minter, minter)
1.501988242956056762e+18

julia> core.price
290000000000

julia> core.leverage_limit
800000

julia> core.leverage()
499751

julia> alice = Wallet("0x2")
Wallet("0x2", 0)

julia> core.peg.send(1e18, minter, alice)
1000000000000000000

julia> core.peg.balanceOf(alice)
1000000000000000000

```

When interacting directl with core, note that amounts are given in wei, so multiply desired number of ETH by 1e18.
For demonstration purposes, you can simply change the price by assigning it a new value:

``` julia
julia> core.leverage() / 1e6
0.5

julia> core.price / PEG_DEMO.ETH_PRICE_DECIMAL
2900.0

julia> core.price = 150.88 |> price_convert_int
15088000000

julia> core.price / PEG_DEMO.ETH_PRICE_DECIMAL
150.8799999999999999999999999999999999999999999999999999999999999999999999999994

julia> core.leverage() / 1e6
9.610286000000000000000000000000000000000000000000000000000000000000000000000017

julia> core.leverage() / 1e6 |> Float64
9.610286


```

## Simulation and Plots
To run simulation, you can use the `sim.jl` script. To generate output csv files only; simply
run the from the command line (it should take care of installing dependencies). Use the '-i' flag for interactive mode.

``` julia
> julia -i sim.jl
```

To run the video/plot script, first uncomment the line to add `CairoMakie` in utils/dependencies.jl.
Then, use "include" from an already-running REPL that has initialized
using the sim script:

``` julia
❯ julia -i sim.jl
  Activating new project at `/var/folders/43/8dxssr2n4ds1zxzb04gt35g00000gn/T/jl_1081zv`
    Updating registry at `~/.julia/registries/General.toml`
   Resolving package versions...
    Updating `/private/var/folders/43/8dxssr2n4ds1zxzb04gt35g00000gn/T/jl_1081zv/Project.toml`
.
.
.
.
[ Info: Beginning simulation
[ Info: Simulation output saved successfully
[ Info: Simulation output saved successfully
        _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.7.2 (2022-02-06)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

julia> include("videoplot.jl")
[ Info: Setting up plot
to generate video, enter `make_video()`
to gnerate a simple plot, enter `simple_plot()`

julia> make_video()
[ Info: Begining to plot video
[ Info: Video plot saved as layout_animation.mp4

julia>

```

The plotting tools can take a little bit to initialize.
